﻿import React, { Component } from "react";
import axios from 'axios';


class WaiverList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            source: props.source,
            data: []
        };
    }

    componentDidMount() {

        var th = this;
        this.serverRequest =
            axios.get(this.state.source)
                .then(function (result) {
                    th.setState({
                        data: result.data
                    });
                })
    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    render() {
        return (
            <div className="list-group">
                {this.state.data.map(function (item) {
                    return (
                        <div key={item.id} className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{item.title} <small>({item.category.name})</small></h3>
                            </div>
                            <div className="panel-body">
                                {item.event} {item.eventDate}
                                <a href={item.link} className="pull-right">
                                    Details
                                </a>
                            </div>
                        </div>
                    );
                })}
            </div>
        )
    }
}

export default WaiverList;
