﻿import React, { Component } from "react";


class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            employeeName: props.employeeName,
        };
    }

    render() {
        return (
            <div className="jumbotron">
                <div className="page-header">
                    <h2>Code of Conduct Compliance System <small className="pull-right">{this.state.employeeName}</small></h2>
                </div>
            </div>
        )
    }
}

export default Header;
