﻿import React, { Component } from "react";
import ReactDOM from "react-dom";

import WaiverList from '../shared/waiverList';

class Waivers extends Component {

    constructor(props) {
        super(props);

        this.state = {
            url: props.apiUrl
        };
    }

    render() {
        return (
            <div>
                <WaiverList source={this.state.url} />
            </div>
        )
    }
}

export default Waivers;
