﻿import React, { Component } from "react";
import ReactDOM from "react-dom";

class Waiver extends Component {

    constructor(props) {
        super(props);

        this.state = {
            apiUrl: props.apiUrl,
            data: []
        };
    }

    render() {
        return (
            <div>
                Detail view of {this.state.apiUrl} (container of another set of components)
            </div>
        )
    }
}

export default Waiver;
