﻿import React, { Component } from "react";
import ReactDOM from "react-dom";

import Header from '../shared/header';
import Waiver from './waiver';
import Waivers from './waivers';

const apiUrl = 'http://localhost/react.poc.api/api/list';

class Emp extends Component{

    constructor(props) {
        super(props);

        this.state = {
            apiUrl: props.source
        };
    }

    render() {
        return (
            <div>
                <Header employeeName="Kim Employee" />

                <Waiver apiUrl={this.state.apiUrl + "1"} />
                <Waivers apiUrl={this.state.apiUrl} />
            </div>
        )
    }
}

ReactDOM.render(<Emp source={apiUrl} />, document.getElementById("app"));
