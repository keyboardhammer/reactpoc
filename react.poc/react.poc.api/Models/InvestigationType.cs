﻿namespace react.client.webapi.service.Models
{
    public class InvestigationType
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}