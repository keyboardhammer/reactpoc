﻿using System;
using System.ComponentModel.DataAnnotations;

namespace react.client.webapi.service.Models
{
    public class Waiver
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public InvestigationType Category { get; set; }

        [Required]
        public string Event { get; set; }

        [Required]
        public DateTime EventDate { get; set; }

        [Required]
        public DateTime EffectiveUntil { get; set; }

        public Security Security { get; set; }
    }
}
