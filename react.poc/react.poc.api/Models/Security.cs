﻿namespace react.client.webapi.service.Models
{
    public class Security
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public string Cusip { get; set; }
        public string Type { get; set; }
    }
}