﻿using System;
using System.Collections.Generic;

namespace react.client.webapi.service.Models
{
    public class List
    {
        public static List<Waiver> Items
        {
            get
            {
                return new List<Waiver>
                {
                    new Waiver {Id=1, Title="Laptop Prize Won...", Category=new InvestigationType{Code="BC", Name="Business Courtesy"}, Event="Request", EventDate=DateTime.Today.AddMonths(-1), EffectiveUntil=DateTime.Today.AddMonths(3), Security=new Security{Symbol="KODK", Name="Eastman Kodak Company"} },
                    new Waiver {Id=2, Title="Citibank", Category=new InvestigationType{Code="PB", Name="Prohibited Company"}, Event="Code of Conduct published a Disposition Schedule", EventDate=DateTime.Today.AddDays(-35), EffectiveUntil=DateTime.Today.AddDays(28), Security=new Security{Symbol="AAPL", Name="Apple"} },
                    new Waiver {Id=3, Title="Annual certification delayed", Category=new InvestigationType{Code="OT", Name="OTHER"}, Event="Close out 50% of position", EventDate=DateTime.Today.AddDays(-1), EffectiveUntil=DateTime.Today.AddDays(55), Security=new Security{Symbol="MSFT", Name="Microsoft Corporation"} },
                };
            }
        }
    }
}