﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

using react.client.webapi.service.Models;

namespace react.client.webapi.service.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ItemController : ApiController
    {
        // GET: api/Item/5
        public Waiver Get(int id)
        {
            return List.Items.Single(x => x.Id == (id % 3 == 0 ? 3 : id % 3));
        }

        // POST: api/Item
        public void Post([FromBody]Waiver value)
        {
        }

        // PUT: api/Item/5
        public void Put(int id, [FromBody]Waiver value)
        {
        }

        // DELETE: api/Item/5
        public void Delete(int id)
        {
        }
    }
}
