﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

using react.client.webapi.service.Models;

namespace react.client.webapi.service.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ListController : ApiController
    {
        // GET: api/List
        public IEnumerable<Waiver> Get()
        {
            return List.Items;
        }
    }
}
